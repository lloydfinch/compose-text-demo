package com.example.compose

import android.content.Context
import android.widget.Toast
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.shape.ZeroCornerSize
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.TextField
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Clear
import androidx.compose.material.icons.filled.Search
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.graphics.drawscope.DrawScope
import androidx.compose.ui.graphics.drawscope.DrawStyle
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp


/**
 * Name: TextFieldApi
 * Author: xchl_wwq
 * Function: TextFieldApi
 * Date: 2021/7/7 10:37 上午
 * Modify: xchl_wwq 2021/7/7 10:37 上午
 */

//fun TextField(
//    value: String, // 文字，也可以传入TextFieldValue
//    onValueChange: (TextFieldValue) -> Unit, // 文字改变的回调
//    modifier: Modifier = Modifier, // 修饰符
//    enabled: Boolean = true, // 是否可用，等价于Android中的enable属性
//    readOnly: Boolean = false, // 是否只读
//    textStyle: TextStyle = LocalTextStyle.current, // 文字格式，可以传入SpanStyle和ParagraphStyle
//    label: @Composable (() -> Unit)? = null, // 标签，跟Material中的label类似
//    placeholder: @Composable (() -> Unit)? = null, // 输入文本为空的占位符，有焦点才会展示
//    leadingIcon: @Composable (() -> Unit)? = null, // 头部图标
//    trailingIcon: @Composable (() -> Unit)? = null, // 尾部图标
//    isError: Boolean = false, // 指定当前输入文本是否出错，如果为错，则会把文字和线框显示为红色来提示
//    visualTransformation: VisualTransformation = VisualTransformation.None, // 可以简单的理解为EditText中的inputType
//    keyboardOptions: KeyboardOptions = KeyboardOptions.Default, // 定义软键盘上的返回键的功能，可以定义为return/search等
//    keyboardActions: KeyboardActions = KeyboardActions(), // 按下软键盘上返回键的回调
//    singleLine: Boolean = false, // 是否单行显示
//    maxLines: Int = Int.MAX_VALUE, // 最大行数
//    interactionSource: MutableInteractionSource = remember { MutableInteractionSource() }, // 表示一个由组件发出的交互流
//    shape: Shape = MaterialTheme.shapes.small.copy(bottomEnd = ZeroCornerSize, bottomStart = ZeroCornerSize), // 定义此文本框的形状(不包含背景)
//    colors: TextFieldColors = TextFieldDefaults.textFieldColors() // 用来定义文字，光标等的处于不同状态的颜色
//)

@Preview
@Composable
fun PreviewTextField() {
    //TextFieldDemo()
}

@Composable
fun TextFieldDemo(context: Context) {
    // 定义一个可观测的text，用来在TextField中展示
    var text by remember {
        mutableStateOf("")
    }
    TextField(
        value = text, // 显示文本
        onValueChange = { text = it }, // 文字改变时，就赋值给text
        label = { Text(text = "Input") }, // label是Input
        // 头部图标，设置为搜索
        leadingIcon = @Composable {
            Image(
                imageVector = Icons.Filled.Search, // 搜索图标
                contentDescription = null,
                modifier = Modifier.clickable { Toast.makeText(context, "search $text", Toast.LENGTH_SHORT).show() }) // 给图标添加点击事件，点击就吐司提示内容
        },
        // 尾部图标，设置为清除
        trailingIcon = @Composable {
            Image(imageVector = Icons.Filled.Clear, // 清除图标
                contentDescription = null,
                modifier = Modifier.clickable { text = "" }) // 给图标添加点击事件，点击就清空text
        },
        placeholder = @Composable { Text(text = "This is placeholder") }, // 不输入内容时的占位符
        visualTransformation = PasswordVisualTransformation(), // 展示为密文
        keyboardOptions = KeyboardOptions(imeAction = ImeAction.Search), // 将键盘的回车键定义为搜索
        // 给回车键定义点击搜索事件，弹出搜索内容
        keyboardActions = KeyboardActions(onSearch = { Toast.makeText(context, "search $text", Toast.LENGTH_SHORT).show() }),
        singleLine = true, // 重新定义回车键，一定要定义为单行，否则回车键还是换行，重定义不生效

        shape = RoundedCornerShape(16.dp), // 指定圆角矩形

        isError = false, // 不展示错误提示
        // 指定下划线颜色
        colors = TextFieldDefaults.textFieldColors(
            focusedIndicatorColor = Color.Transparent, // 有焦点时的颜色，透明
            unfocusedIndicatorColor = Color.Green, // 无焦点时的颜色，绿色
            errorIndicatorColor = Color.Red, // 错误时的颜色，红色
            disabledIndicatorColor = Color.Gray // 不可用时的颜色，灰色
        )
    )
}


@Composable
fun OutlinedTextFieldDemo() {
    var text by remember { mutableStateOf("") }
    OutlinedTextField(value = text,
        label = { Text(text = "Input something") },
        onValueChange = { text = it })
}

@Composable
fun BasicTextFieldDemo() {
    var text by remember { mutableStateOf("hello") }
    BasicTextField(
        value = text,
        onValueChange = { text = it },
        modifier = Modifier.background(Color.Green, RoundedCornerShape(8.dp)), // 加一个绿色背景

        // 画修饰框
        decorationBox = @Composable { innerTextField ->
            // 先画个圆压压惊
            Canvas(modifier = Modifier.size(width = 100.dp, height = 50.dp)) {
                drawCircle(color = Color.Red, style = Stroke(width = 1F))
            }
            // 然后再画文字
            innerTextField()
        }
    )
}

