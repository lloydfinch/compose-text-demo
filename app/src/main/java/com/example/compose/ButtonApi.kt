package com.example.compose

import android.content.Context
import android.widget.Toast
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.Button
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberUpdatedState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

/**
 * Name: ButtonApi
 * Author: xchl_wwq
 * Function: ButtonApi
 * Date: 2021/7/11 3:01 下午
 * Modify: xchl_wwq 2021/7/11 3:01 下午
 */
@Preview
@Composable
fun PreviewButton() {

}

//fun Button(
//    onClick: () -> Unit, // 点击事件
//    modifier: Modifier = Modifier, // 修饰符
//    enabled: Boolean = true, // 是否可用
//    interactionSource: MutableInteractionSource = remember { MutableInteractionSource() }, 表示一个由组件发出的交互流
//    elevation: ButtonElevation? = ButtonDefaults.elevation(), // 梯度
//    shape: Shape = MaterialTheme.shapes.small, // 按钮的形状，对背景不生效
//    border: BorderStroke? = null, // 按钮的边框，对背景不生效
//    colors: ButtonColors = ButtonDefaults.buttonColors(), // 在不同状态下按钮和背景的颜色
//    contentPadding: PaddingValues = ButtonDefaults.ContentPadding, // 内边距，默认上下8dp，左右16dp
//    content: @Composable RowScope.() -> Unit // 内部内容(可以把Button理解为一个FrameLayout，content就是子View)
//)

@Composable
fun ButtonDemo(context: Context) {
    ButtonDemo1(context = context)
}


@Composable
fun ButtonDemo1(context: Context) {
    Button(
        onClick = { Toast.makeText(context, "Click Button", Toast.LENGTH_SHORT).show() },
        shape = RoundedCornerShape(16.dp), // 16dp的圆角
        border = BorderStroke(2.dp, Color.DarkGray), // 灰色边框
        modifier = Modifier.background(Color.Green), //给Button添加绿色背景
        contentPadding = PaddingValues(40.dp), // 添加内边距
        colors = object : ButtonColors { // 添加颜色
            @Composable
            override fun backgroundColor(enabled: Boolean): State<Color> {
                // 内容的背景颜色: 可用时为黄色，不可用为灰色
                return rememberUpdatedState(if (enabled) Color.Yellow else Color.Gray)
            }

            @Composable
            override fun contentColor(enabled: Boolean): State<Color> {
                // 内容的内容颜色: 可用时为绿色，不可用时为蓝色
                return rememberUpdatedState(if (enabled) Color.Green else Color.Blue)
            }
        },
        enabled = false
    ) {
        Text(text = "Button", modifier = Modifier.background(Color.Red))
    }
}

@Composable
fun ButtonDemo2(context: Context) {

    // 点击事件
    val onClick = { Toast.makeText(context, "Click Button", Toast.LENGTH_SHORT).show() }

    val sizeModifier = Modifier.size(width = 32.dp, height = 32.dp)

    Column(verticalArrangement = Arrangement.Center, horizontalAlignment = Alignment.CenterHorizontally, modifier = Modifier.fillMaxSize()) {

        // 文字按钮
        Button(onClick = onClick, elevation = ButtonDefaults.elevation(pressedElevation = 8.dp)) {
            Text(text = "TextButton")
        }

        Spacer(modifier = Modifier.size(width = 10.dp, height = 8.dp))

        Button(
            onClick = { Toast.makeText(context, "Click Button", Toast.LENGTH_SHORT).show() },
            shape = RoundedCornerShape(8.dp),
            border = BorderStroke(2.dp, color = Color.Magenta)
        ) {
            Text(text = "RoundedBorder")
        }

        Spacer(modifier = Modifier.size(width = 10.dp, height = 8.dp))

        // 图片按钮
        Button(onClick = onClick) {
            Image(
                painter = painterResource(id = R.drawable.ic_google),
                contentDescription = "ImageButton", modifier = sizeModifier
            )
        }

        Spacer(modifier = Modifier.size(width = 10.dp, height = 8.dp))

        // 图上字下
        Button(onClick = onClick) {
            Column(verticalArrangement = Arrangement.Center, horizontalAlignment = Alignment.CenterHorizontally) {
                Image(painter = painterResource(id = R.drawable.ic_google), contentDescription = "ImageButton", modifier = sizeModifier)
                Text(text = "图上字下")
            }
        }

        Spacer(modifier = Modifier.size(width = 10.dp, height = 8.dp))

        // 图下字上
        Button(onClick = onClick) {
            Column(verticalArrangement = Arrangement.Center, horizontalAlignment = Alignment.CenterHorizontally) {
                Text(text = "图下字上")
                Image(painter = painterResource(id = R.drawable.ic_google), contentDescription = "ImageButton", modifier = sizeModifier)
            }
        }

        Spacer(modifier = Modifier.size(width = 10.dp, height = 8.dp))

        // 图左字右
        Button(onClick = onClick) {
            Row(horizontalArrangement = Arrangement.Center, verticalAlignment = Alignment.CenterVertically) {
                Image(painter = painterResource(id = R.drawable.ic_google), contentDescription = "ImageButton", modifier = sizeModifier)
                Text(text = "图左字右")
            }
        }

        Spacer(modifier = Modifier.size(width = 10.dp, height = 8.dp))

        // 图右字左
        Button(onClick = onClick) {
            Row(horizontalArrangement = Arrangement.Center, verticalAlignment = Alignment.CenterVertically) {
                Text(text = "图右字左")
                Image(painter = painterResource(id = R.drawable.ic_google), contentDescription = "ImageButton", modifier = sizeModifier)
            }
        }
    }
}
