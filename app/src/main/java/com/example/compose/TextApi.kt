package com.example.compose

import android.content.Context
import android.widget.Toast
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.text.ClickableText
import androidx.compose.foundation.text.selection.DisableSelection
import androidx.compose.foundation.text.selection.SelectionContainer
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shadow
import androidx.compose.ui.text.*
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.*
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

/**
 * Name: TextApi
 * Author: lloydfinch
 * Function: TextApi
 * Date: 2021/7/5 4:13 下午
 * Modify: lloydfinch 2021/7/5 4:13 下午
 */

@Preview
@Composable
fun PreviewScreen() {
    //TextDemo()
    //TextDemo2()
    //TextDemo3()
    TextDemo4()
}

/**
 * 基础功能
 */
@Composable
fun TextDemo() {
    //stringResource(id = R.string.hello)
    val text = "this is compose text demo, which likes TextView in android native xml layout"
    Text(
        text = text, // 文字
        color = Color.Green, // 字体颜色
        fontSize = 16.sp, // 字体大小
        fontStyle = FontStyle.Italic, // 斜体
        fontWeight = FontWeight.Bold, // 粗体
        textAlign = TextAlign.Center, // 对齐方式: 居中对齐
        modifier = Modifier.width(300.dp), // 宽高
        maxLines = 2, // 最大行数
        overflow = TextOverflow.Ellipsis, // 文字溢出后就裁剪
        softWrap = true, // 文字过长时是否换行
        textDecoration = TextDecoration.Underline, // 文字装饰，这里添加下划线
        fontFamily = FontFamily.Cursive, // 字体样式
        lineHeight = 40.sp, // 行高
        letterSpacing = 5.sp // 字符间距
    )
}

/**
 * 富文本
 */
@Composable
fun TextDemo2() {
    Text(buildAnnotatedString {
        // 使用白色背景，红色字体，18sp，Monospace字体来绘制"Hello " (注意后面有个空格)
        withStyle(style = SpanStyle(color = Color.Red, background = Color.White, fontSize = 18.sp, fontFamily = FontFamily.Monospace)) {
            append("Hello ")
        }
        // 正常绘制"World"
        append("World ")
        // 使用黄色背景，绿色字体，18sp，Serif字体，W900粗体来绘制"Click"
        withStyle(style = SpanStyle(color = Color.Green, background = Color.Yellow, fontSize = 30.sp, fontFamily = FontFamily.Serif, fontWeight = FontWeight.W900)) {
            append("Click")
        }
        // 正常绘制" Me" (注意前面有个空格)
        append(" Me")

        // 添加阴影及几何处理
        withStyle(
            style = SpanStyle(
                color = Color.Yellow,
                background = Color.White,
                baselineShift = BaselineShift(1.0f), // 向BaseLine上偏移10
                textGeometricTransform = TextGeometricTransform(scaleX = 2.0F, skewX = 0.5F), // 水平缩放2.0，并且倾斜0.5
                shadow = Shadow(color = Color.Blue, offset = Offset(x = 1.0f, y = 1.0f), blurRadius = 10.0f) // 添加音阴影和模糊处理
            )
        ) {
            append(" Effect")
        }
    })
}


/**
 * 段落
 */
@Composable
fun TextDemo3() {
    Text(buildAnnotatedString {
        // 指定对齐方式为Start，通过textIndent指定第一行每段第一行缩进32sp，其余行缩进8sp
        withStyle(style = ParagraphStyle(textAlign = TextAlign.Start, textIndent = TextIndent(firstLine = 32.sp, restLine = 8.sp))) {

            // 第一段，因为只有一行，所以直接缩进32sp
            withStyle(style = SpanStyle(color = Color.Red)) {
                append("Hello, this is first paragraph\n")
            }
            // 第二段(第一行会缩进32sp，后续每行会缩进8sp)
            withStyle(style = SpanStyle(color = Color.Green, fontWeight = FontWeight.Bold)) {
                append("Hello, this is second paragraph,very long very long very long very long very long very long very long very long very long very long\n")
            }
            // 第三段，因为只有一行，所以直接缩进32sp
            append("Hello, this is third paragraph\n")
        }
    })
}

/**
 * 可选/不可选
 */
@Composable
fun TextDemo4() {
    // 设置可选区域
    SelectionContainer {
        // Column等价于竖直的LinearLayout
        Column {
            Text(text = "可以选中我，可以选中我，可以选中我")

            // 设置不可选区域
            DisableSelection {
                Text(text = "选不中我，选不中我，选不中")
            }

            // 位于可选区域内，可选
            Text(text = "可以选中我，可以选中我，可以选中我")
        }
    }
}

/**
 * 响应点击
 */
@Composable
fun TextDemo5(context: Context) {
    ClickableText(text = AnnotatedString("请点击我"), onClick = { index ->
        Toast.makeText(context, "点击位置:$index", Toast.LENGTH_SHORT).show()
    })
}

/**
 * 点击打开超链接
 */
@Composable
fun TextDemo6(context: Context) {

    // 构建注解文本
    val url_tag = "article_url";
    val articleText = buildAnnotatedString {
        append("点击")

        // pushStringAnnotation()表示开始添加注解，可以理解为构造了一个<tag,annotation>的映射
        pushStringAnnotation(tag = url_tag, annotation = "https://devloper.android.com")
        // 要添加注解的文本为"打开本文"
        withStyle(style = SpanStyle(color = Color.Blue, fontWeight = FontWeight.Bold)) {
            append("展示Android官网")
        }
        // pop()表示注解结束
        pop()
    }

    // 构造可点击文本
    ClickableText(text = articleText, onClick = { index ->
        // 根据tag取出annotation并打印
        articleText.getStringAnnotations(tag = url_tag, start = index, end = index).firstOrNull()?.let { annotation ->
            Toast.makeText(context, "点击了:${annotation.item}", Toast.LENGTH_SHORT).show()
        }
    })
}