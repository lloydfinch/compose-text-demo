package com.example.compose

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.DefaultAlpha
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

/**
 * Name: ImageApi
 * Author: lloydfinch
 * Function: ImageApi
 * Date: 2021/7/6 10:36 上午
 * Modify: lloydfinch 2021/7/6 10:36 上午
 */
//fun Image(
//    painter: Painter,
//    contentDescription: String?,
//    modifier: Modifier = Modifier,
//    alignment: Alignment = Alignment.Center,
//    contentScale: ContentScale = ContentScale.Fit,
//    alpha: Float = DefaultAlpha,
//    colorFilter: ColorFilter? = null
//)

@Preview
@Composable
fun PreviewImage() {
    ImageDemo()
}

/**
 * 图片展示
 */
@Composable
fun ImageDemo() {
    Image(
        painter = painterResource(id = R.mipmap.head),
        contentDescription = stringResource(id = R.string.str_image_description),
        modifier = Modifier
            .size(width = 100.dp, height = 80.dp)
            .clip(CircleShape), // 这是为圆形裁剪
        contentScale = ContentScale.FillBounds, // 等价于ImageView的fitXY
        alignment = Alignment.Center, // 居中显示
    )
}

/**
 * 绘制
 */
@Composable
fun DrawDemo() {

}
